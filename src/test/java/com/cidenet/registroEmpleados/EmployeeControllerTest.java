package com.cidenet.registroEmpleados;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.cidenet.registroEmpleados.controller.EmployeeController;
import com.cidenet.registroEmpleados.entity.Employee;
import com.cidenet.registroEmpleados.service.EmployeeService;

public class EmployeeControllerTest {

	@InjectMocks
	private EmployeeController employeeController = new EmployeeController();

	@Mock
	EmployeeService employeeService;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validarListadoEmpleados() {

		// Arrange
		Employee employee = new Employee();
		List<Employee> employeeList = new ArrayList<>();
		employee.setId("1234");
		employeeList.add(employee);

		Mockito.when(employeeService.listEmployees()).thenReturn(employeeList);

		// Act
		ResponseEntity<List<Employee>> employeeRes = employeeController.listEmployees();

		// Assert
		Assert.assertEquals(employeeRes.getBody().get(0).getId(), employeeList.get(0).getId());

	}

	@Test
	public void validarListadoEmpleadosVacio() {

		// Arrange
		List<Employee> employeeList = new ArrayList<>();

		Mockito.when(employeeService.listEmployees()).thenReturn(null);
		
		// Act
		ResponseEntity<List<Employee>> employeeRes = employeeController.listEmployees();

		// Assert
		Assert.assertEquals(employeeRes.getBody(), employeeList);
		Assert.assertEquals(true, employeeList.isEmpty());

	}

	@Test
	public void validateListEmpleyeesPaginateSuccess() {
		// Arrange
		List<Employee> employeeList = new ArrayList<>();

		Employee employee = new Employee();
		employee.setId("123");
		employee.setIdNumber("1234");
		employeeList.add(employee);

		Mockito.when(employeeService.listEmployeesPaginate(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(employeeList);

		// Act
		List<Employee> agreementListResponse = employeeController.listEmployeesPaginate(1, 1)
				.getBody();

		// Assert
		Assert.assertEquals(agreementListResponse.size(), 1);
	}

	@Test
	public void validateListEmpleyeesPaginateNoEmpleyees() {
		// Arrange

		Mockito.when(employeeService.listEmployeesPaginate(Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
		
		// Act
		List<Employee> employeeListResponse = employeeController.listEmployeesPaginate(1, 1)
				.getBody();

		// Assert
		Assert.assertEquals(employeeListResponse.size(), 0);
	}

	@Test
	public void validateListEmpleyeesPaginateEmpty() {
		// Arrange
		List<Employee> employeeList = new ArrayList<>();
		Mockito.when(employeeService.listEmployeesPaginate(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(employeeList);
		
		// Act
		List<Employee> employeeListResponse = employeeController.listEmployeesPaginate(1, 1)
				.getBody();

		// Assert
		Assert.assertEquals(employeeListResponse.size(), 0);
	}

	@Test
	public void validarAdicionEmpleado() {
		// Arrange
		Employee employee = new Employee();
		employee.setId("123");
		employee.setIdNumber("1234");
		Mockito.when(employeeService.saveEmployee(Mockito.any())).thenReturn(employee);
		
		// Act
		ResponseEntity<Employee> employeeRes = employeeController.saveEmployee(new Employee());

		// Assert
		Assert.assertEquals(employeeRes.getBody(), employee);

	}

	@Test
	public void validarErrorAdicionEmpleado() {
		// Arrange
		Employee employee = new Employee();
		employee.setId("123");
		employee.setIdNumber("1234");
		Mockito.when(employeeService.saveEmployee(Mockito.any())).thenReturn(employee);

		// Act
		ResponseEntity<Employee> employeeRes = employeeController.saveEmployee(null);

		// Assert
		Assert.assertEquals(employeeRes.getBody().getId(), null);
	}

	@Test
	public void validarEmpleadoPorNumberId() {
		// Arrange

		Employee employee = new Employee();
		employee.setId("123");
		employee.setIdNumber("1234");

		Mockito.when(employeeService.getEmployeeById(Mockito.anyString())).thenReturn(employee);
		
		// Act
		ResponseEntity<Employee> employeeRes = employeeController.getEmployeeById("1234");

		// Assert
		Assert.assertEquals(employeeRes.getBody().getIdNumber(), employee.getIdNumber());

	}


}
