package com.cidenet.registroEmpleados.service;

import java.util.List;

import com.cidenet.registroEmpleados.entity.Employee;

public interface EmployeeService {

	/**
	 * Metodo que retorna todos los empleados
	 * 
	 * @return
	 */
	public List<Employee> listEmployees();


	/**
	 * Metodo que retorna todos los empleados paginados
	 * 
	 * @return List<Agreement>
	 * 
	 */
	public List<Employee> listEmployeesPaginate(int page, int size);

	/**
	 * Metodo que retorna un empleado filtrado por el ID
	 * 
	 * @param agreementId
	 * @return
	 */
	public Employee getEmployeeById(String idNumber);

	/**
	 * Metodo que actualiza un empleado en la BD
	 * 
	 * @param agreement
	 * @return
	 */
	public Employee saveEmployee(Employee employee);

	/**
	 * Metodo que crea un empleado en la BD
	 * 
	 * @param agreement
	 * @return
	 */
	public Employee createEmployee(Employee employee);

	/**
	 * Metodo que elimina de manera lagica un convenio en la BD
	 * 
	 * @param agreementId
	 */
	public void deleteEmployee(String idNumber);

}
