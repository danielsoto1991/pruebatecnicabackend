package com.cidenet.registroEmpleados.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.cidenet.registroEmpleados.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, String> {

	/**
	 * Metodo que retorna los empelados
	 * 
	 * @return
	 */
	public List<Employee> findAll();
	
	/**
	 * Metodo que retorna los empelados paginados
	 * 
	 * @return
	 */
	public Page<Employee> findAll(Pageable pageable);
	
	/**
	 * Metodo que retorna un empelado en especifico
	 * 
	 * @param idNumber
	 * @return
	 */
	public Employee findByIdNumber(String idNumber);
	
	/**
	 * Metodo que retorna un empleado por email
	 * 
	 * @param idNumber
	 * @return
	 */
	public Employee findByEmail(String email);
	
}
