package com.cidenet.registroEmpleados.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;

import com.cidenet.registroEmpleados.entity.Employee;
import com.cidenet.registroEmpleados.repository.EmployeeRepository;
import com.cidenet.registroEmpleados.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {


	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public List<Employee> listEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public List<Employee> listEmployeesPaginate(int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return Lists.newArrayList(employeeRepository.findAll(pageable));
	}

	@Override
	public Employee getEmployeeById(String idNumber) {
		return employeeRepository.findByIdNumber(idNumber);
	}

	@Override
	public Employee saveEmployee(Employee employee) {
		Optional<Employee> optinalEntity = employeeRepository.findById(employee.getId());
		Employee lastEmployee = optinalEntity.get();
		
		Employee employeeSaved = null;
		if (!lastEmployee.getFirstName().equals(employee.getFirstName()) || !lastEmployee.getFirstLastName().equals(employee.getFirstLastName()) ||
				!lastEmployee.getJobCountry().equals(employee.getJobCountry())) {
			int number = 0;
			String country = "";
			if("Colombia".equals(employee.getJobCountry())) {
				country = "co";
			} else {
				country = "us";
			}
			String email = employee.getFirstName().toLowerCase() + "." + employee.getFirstLastName().replace(" ","").toLowerCase() + "@cidenet.com." + country; 
			
			employee.setEmail(validateEmail(employee, email, number, country));
		}
		if (null == employeeRepository.findByIdNumber(employee.getIdNumber())) {
			employeeSaved = employeeRepository.save(employee);
		}else if (lastEmployee.getIdNumber().equals(employee.getIdNumber())) {
			employeeSaved = employeeRepository.save(employee);
		}
		
		return employeeSaved;
	}

	@Override
	public Employee createEmployee(Employee employee) {
		Employee employeeCreated = null;
		Random rand = new Random();
		employee.setId(String.valueOf(rand.nextLong()));
		int number = 0;
		String country = "";
		if("Colombia".equals(employee.getJobCountry())) {
			country = "co";
		} else {
			country = "us";
		}
		String email = employee.getFirstName().toLowerCase() + "." + employee.getFirstLastName().replace(" ","").toLowerCase() + "@cidenet.com." + country; 
		
		employee.setEmail(validateEmail(employee, email, number, country));
		
		if (null == employeeRepository.findByIdNumber(employee.getIdNumber())) {
			employeeCreated = employeeRepository.save(employee);
		}
		return employeeCreated;
	}

	@Override
	public void deleteEmployee(String idNumber) {
		employeeRepository.delete(employeeRepository.findByIdNumber(idNumber));
	}
	
	private String validateEmail(Employee employee, String email, int number, String country) {
		
		while (null != employeeRepository.findByEmail(email)) {
			number++;
			email = employee.getFirstName().toLowerCase() + "." + employee.getFirstLastName().replace(" ","").toLowerCase() + "." + String.valueOf(number) + "@cidenet.com." + country;
		}
		
		return email;
	}

}
