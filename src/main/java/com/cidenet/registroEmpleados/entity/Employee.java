package com.cidenet.registroEmpleados.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Employee implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4085669212762406946L;

	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "numero_identificacion", length = 20)
	private String idNumber;

	@Column(name = "tipo_identificacion", length = 30)
	private String idType;

	@Column(name = "primer_apellido", length = 20)
	private String firstLastName;
	
	@Column(name = "segundo_apellido", length = 20)
	private String secondLastName;

	@Column(name = "primer_nombre", length = 20)
	private String firstName;
	
	@Column(name = "otros_nombres", length = 50)
	private String otherNames;

	@Column(name = "pais_empleo", length = 20)
	private String jobCountry;

	@Column(name = "correo", length = 300)
	private String email;
	
	@Column(name = "fecha_ingreso", length = 50)
	private String admissionDate;
	
	@Column(name = "area", length = 30)
	private String area;
	
	@Column(name = "estado", length = 20)
	private String state;
	
	@Column(name = "fecha_creacion", length = 50)
	private String creationDate;

	@Column(name = "fecha_modificacion", length = 50)
	private String modificationDate;

	public Employee(String id, String idNumber, String idType, String firstLastName, String secondLastName, String firstName,
			String otherNames, String jobCountry, String email, String admissionDate, String area, String state,
			String creationDate, String modificationDate) {
		super();
		this.id = id;
		this.idNumber = idNumber;
		this.idType = idType;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.firstName = firstName;
		this.otherNames = otherNames;
		this.jobCountry = jobCountry;
		this.email = email;
		this.admissionDate = admissionDate;
		this.area = area;
		this.state = state;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public Employee() {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public String getJobCountry() {
		return jobCountry;
	}

	public void setJobCountry(String jobCountry) {
		this.jobCountry = jobCountry;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}
	
}