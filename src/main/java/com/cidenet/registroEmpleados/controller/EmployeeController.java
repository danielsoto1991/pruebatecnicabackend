package com.cidenet.registroEmpleados.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.registroEmpleados.entity.Employee;
import com.cidenet.registroEmpleados.service.EmployeeService;

@RestController
@RequestMapping("/employee")
@CrossOrigin(origins= "http://localhost:4200", maxAge=3600)
public class EmployeeController {

	HttpHeaders headers;

	@Autowired
	EmployeeService employeeService;

	public EmployeeController() {
		headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
	}

	/**
	 * Metodo que lista los empleados 
	 * 
	 * @return List<Employee>
	 */
	@GetMapping(path = "/list")
	public @ResponseBody ResponseEntity<List<Employee>> listEmployees() {
		
		List<Employee> employeeList = employeeService.listEmployees();

		if (null != employeeList && !employeeList.isEmpty()) {
			return new ResponseEntity<>(employeeList, headers, HttpStatus.OK);
		} else {
			employeeList = new ArrayList<>();
			return new ResponseEntity<>(employeeList, headers,
					HttpStatus.NO_CONTENT);
		}
	}


	/**
	 * Metodo que lista los empleados paginados
	 * 
	 * @return List<Employee>
	 */
	@GetMapping(path = "/list-paginate")
	public @ResponseBody ResponseEntity<List<Employee>> listEmployeesPaginate(
			@RequestParam int page, @RequestParam int size) {

		List<Employee> employeeList = employeeService.listEmployeesPaginate(page, size);

		if (null != employeeList && !employeeList.isEmpty()) {
			return new ResponseEntity<>(employeeList, headers, HttpStatus.OK);
		} else {
			employeeList = new ArrayList<>();
			return new ResponseEntity<>(employeeList, headers,
					HttpStatus.NO_CONTENT);
		}
	}
	
	/**
	 * Metodo que crea en BD un registro de empleado
	 * 
	 * @param Employee
	 * @return Employee
	 */
	@PostMapping(path = "/create")
	public @ResponseBody ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {

		Employee employeeSaved = employeeService.createEmployee(employee);
		if (null != employeeSaved) {
			return new ResponseEntity<>(employeeSaved, headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(employeeSaved, headers,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Metodo que guarda en BD un registro de empleado
	 * 
	 * @param Employee
	 * @return Employee
	 */
	@PostMapping(path = "/save")
	public @ResponseBody ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee) {

		Employee employeeSaved = employeeService.saveEmployee(employee);
		if (null != employeeSaved) {
			return new ResponseEntity<>(employeeSaved, headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(employeeSaved, headers,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Metodo que obtiene un empleado filtrado por Id
	 * 
	 * @param numberId
	 * @return Employee
	 */
	@GetMapping(path = "/get")
	public @ResponseBody ResponseEntity<Employee> getEmployeeById(
			@RequestParam String idNumber) {
	
		Employee employee = employeeService.getEmployeeById(idNumber);

		if (null != employee) {
			return new ResponseEntity<>(employee, headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new Employee(), headers,
					HttpStatus.NO_CONTENT);
		}

	}
	
	/**
	 * Metodo que elimina un empleado
	 * 
	 * @param Employee
	 */
	@GetMapping(path = "/delete")
	public @ResponseBody ResponseEntity<String> deleteEmployee(@RequestParam("idNumber") String idNumber) {

		employeeService.deleteEmployee(idNumber);
		
		if (null == employeeService.getEmployeeById(idNumber)) {
			return new ResponseEntity<>(null, headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
